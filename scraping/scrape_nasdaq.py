import json
from bs4 import BeautifulSoup as bs
from threading import Thread
import requests

TICKER_HEAD = 'https://www.nasdaq.com/symbol/{0}/news-headlines?page={1}'


def scrape_nasdaq_ticker_news(ticker, folder='data'):
    """
    scrape nasdaq
    :param ticker: ticker string
    :param folder: data folder
    :return:
    """
    url_page = TICKER_HEAD.format(ticker, '')

    continue_scrape = True

    max_page_id = 1
    current_page = 0
    news_urls = []
    while continue_scrape:
        print(current_page)
        try:
            current_page += 1
            if current_page > max_page_id:
                break

            # http://theautomatic.net/2017/08/24/scraping-articles-about-stocks/
            url = TICKER_HEAD.format(ticker, current_page)
            print(url)
            html = requests.get(url).text
            soup = bs(html, 'lxml')
            links = soup.find_all('a')
            urls = [link.get('href') for link in links]
            urls = [url for url in urls if url is not None]
            '''Filter the list of urls to just the news articles'''
            news_urls = news_urls + [url for url in urls if '/article/' in url]
            if current_page == 1:
                pages_urls = [url for url in urls if url_page in url]
                if len(pages_urls) == 0:
                    break
                pages_num = [int(url.split(url_page)[1]) for url in pages_urls]
                max_page_id = max(pages_num)
                print(max_page_id)
        except Exception as e:
            print(e)
            pass
    news_urls = list(set(news_urls))

    data = []
    if len(news_urls) > 0:
        news_chunks = list(chunks(news_urls, 2))
        for news_chunk in news_chunks:
            result = process_news_url_multithreads(news_chunk)
            result = [x for x in result if x is not None]
            data = data + result
            print('{0} news scraped out of {1}'.format(len(data), len(news_urls)))

    json.dump(data, open(folder + '/' + ticker + '_nasdaq.json', 'w'))
    return data


def process_news_url(news_url):
    try:
        '''Get HTML of the webpage associated with news_url'''
        news_html = requests.get(news_url).text

        '''Convert news_url to a BeautifulSoup object'''
        news_soup = bs(news_html, 'lxml')

        '''Use news_soup to get the text from all pargraphs on page'''
        paragraphs = [par.text for par in news_soup.find_all('p')]

        '''Lastly, join all text in the list above into a single string'''
        news_text = '\n'.join(paragraphs)

        return news_text
    except:
        return None


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        Thread.__init__(self, group, target, name, args, kwargs, daemon=daemon)

        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self):
        Thread.join(self)
        return self._return


def process_news_url_multithreads(news_urls):
    threads = []
    for url in news_urls:
        thread = ThreadWithReturnValue(target=process_news_url, args=(url,))
        thread.start()
        threads.append(thread)

    result = []
    for thread in threads:
        result.append(thread.join())

    return result


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
