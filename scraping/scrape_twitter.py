from twitterscraper import query_tweets
import datetime as dt
from itertools import groupby
from operator import itemgetter
from datetime import timedelta
import json

NUM_TWEETS_PER_WEEK = 500
NUM_TWEETS_TOTAL = 10000
punctuation_list = dict((ord(char), ' ') for char in '!"#%&\'()*+,-./:;<=>?@[\\]^_`{|}~')
MIN_SCRAPING_DATE = dt.date(2017, 1, 1)


def simple_text_to_words(text):
    text = text.lower()
    words = text.translate(punctuation_list)
    words = words.split()
    return words


def get_consecutive_list(data_list):
    consecutive_lists = []
    for k, g in groupby(enumerate(data_list), (lambda x: x[0] - x[1])):
        consec_list = list(map(itemgetter(1), g))

        if consec_list[-1] - consec_list[0] > 1:
            consecutive_lists.append((consec_list[0], consec_list[-1]))
    return consecutive_lists


def keyword_not_in_dollar_stream(words_list, keyword):
    idx_dollars = [idx for idx, token in enumerate(words_list) if token[0] == '$']
    consecutive_dollar_signs = get_consecutive_list(idx_dollars)
    keyword_idx = [idx for idx, token in enumerate(words_list) if token == keyword]
    valid_keyword_idx = []
    for idx in keyword_idx:
        in_range = [x for x in consecutive_dollar_signs if x[0] <= idx <= x[1]]
        if len(in_range) == 0:
            valid_keyword_idx.append(idx)
    return valid_keyword_idx


def scrape_ticker_pos_twitter(ticker, valid_tweets_num=NUM_TWEETS_TOTAL, earliest_date=None):
    """
    Scrape the ticker on twitter
    :param ticker: ticker text
    :param valid_tweets_num: total number of valid tweets to fetch
    :param earliest_date: do not scrape before the specified date
    :return: list of texts
    """

    valid_tweets = []
    garbage_tweets = []

    start_time = dt.date(2018, 11, 10)
    end_time = dt.date(2018, 11, 16)

    while len(valid_tweets) < valid_tweets_num:
        # scrape posts
        print('Start scraping from ', start_time, 'to', end_time)
        tweets = query_tweets(ticker, NUM_TWEETS_PER_WEEK, begindate=start_time, enddate=end_time, lang='en')
        tweets = [x.text for x in tweets]

        # process
        for text in tweets:
            words_list = simple_text_to_words(text)
            valid_keywords = keyword_not_in_dollar_stream(words_list, ticker.lower())
            if len(valid_keywords) == 0:
                garbage_tweets.append(text)
            else:
                valid_tweets.append(text)

        print('Scraped in total {0} valid, {1} garbage tweets'.format(len(valid_tweets), len(garbage_tweets)))

        # update start, end time
        end_time = start_time - timedelta(1)
        start_time = start_time - timedelta(7)

        if earliest_date is not None:
            if end_time < earliest_date:
                break
    garbage_tweets = garbage_tweets[:NUM_TWEETS_TOTAL]
    return valid_tweets, garbage_tweets


def scrape_ticker_neg_twitter(ticker, tweets_num=NUM_TWEETS_TOTAL):
    start_time = dt.date(2018, 11, 10)
    end_time = dt.date(2018, 11, 16)

    all_tweets = []
    while len(all_tweets) < tweets_num:
        # scrape posts
        print('Start scraping from ', start_time, 'to', end_time)
        tweets = query_tweets(ticker, NUM_TWEETS_PER_WEEK, begindate=start_time, enddate=end_time, lang='en')
        tweets = [x.text for x in tweets]
        all_tweets = all_tweets + tweets
        print('Scraped in total {0} tweets'.format(len(all_tweets)))

        # update start, end time
        end_time = start_time - timedelta(1)
        start_time = start_time - timedelta(7)

    return all_tweets


def scrape_ticker(ticker, tweets_num=NUM_TWEETS_TOTAL):

    result_blob = {}

    # scrape positive examples
    result_blob['pos'], result_blob['garbage'] = scrape_ticker_pos_twitter('$' + ticker.upper(),
                                                                           tweets_num,
                                                                           earliest_date=MIN_SCRAPING_DATE)

    # scrape negative examples
    result_blob['neg'] = scrape_ticker_neg_twitter(ticker, tweets_num)

    output_file = 'data/twitter_{0}.json'.format(ticker)
    json.dump(result_blob, open(output_file, 'w'))

    return result_blob
