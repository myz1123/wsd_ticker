from utils.text_preprocessor import TextPreprocessor
from algorithm.model import WSDClassifier
import itertools


class TickerWSD:

    def __init__(self):

        # initialize text preprocessor
        self.text_preprocessor = TextPreprocessor()
        self.clf_model = WSDClassifier()

    def wsd(self, text_list):
        """
        Given a list of documents, per document identify potential tickers and verify whether they are indeed tickers
        :param text_list: list of text documents
        :return: list of potential tickers identified, also indicating whether they are tickers
        """

        # preprocess texts to a set of tokens
        tokens_list = self.text_preprocessor.text_preprocessing(text_list)

        # for each set of tokens, get the potential ticker and context tokens
        tickers_contexts_all_docs = [self.context_tokens_isolation(x) for x in tokens_list]

        # label text idx for better acounting
        doc_idx = 0
        for ticker_context_doc in tickers_contexts_all_docs:
            for ticker_context in ticker_context_doc:
                ticker_context['doc_idx'] = doc_idx
            doc_idx += 1

        # now we can treat each ticker context individually iregardless which document it is from
        ticker_context_list = list(itertools.chain.from_iterable(tickers_contexts_all_docs))

        # run the rule based method ahead of time to determine whether it is a ticker before passing to model
        for ticker_context in ticker_context_list:
            self.simple_method(ticker_context)

        # if there are undecided cases left run model
        ticker_context_rest = [x for x in ticker_context_list if 'is_ticker' not in x]
        if len(ticker_context_rest) > 0:
            self.clf_model.predict_on_context_tokens(ticker_context_rest)

        # return
        return_data_list = []
        for idx in range(doc_idx):

            ticker_context_doc = [x for x in ticker_context_list if x['doc_idx'] == idx]
            result = [
                {
                    'doc_idx': idx,
                    'raw_text': ticker_content['raw_text'],
                    'ticker_text': ticker_content['ticker'],
                    'is_ticker': ticker_content['is_ticker']
                 }
                for ticker_content in ticker_context_doc
            ]
            return_data_list.append(result)
        return return_data_list

    def context_tokens_isolation(self, tokens):
        """
        Given a document, there could be multiple tickers words, isolate each ticker word, use the prev and after
        sentence as context to determine the ticker word
        :param tokens: tokens representing a document
        :return: list of dict of {ticker, ticker_token, relevant_tokens}
        """

        ticker_context_data = []

        for token in tokens:
            potential_ticker = token['potential_ticker']

            if potential_ticker:

                ticker_token = token
                token_idx = token['token_idx']
                sentence_idx = token['sentence_idx']

                # context prev to next sentences
                prev_tokens = [x for x in tokens if x['sentence_idx'] == sentence_idx - 1]
                next_tokens = [x for x in tokens if x['sentence_idx'] == sentence_idx + 1]
                if len(prev_tokens) > 0:
                    prev_ticker_tokens_idx = [x['token_idx'] for x in prev_tokens if x['potential_ticker']]
                    if len(prev_ticker_tokens_idx) > 0:
                        max_id = max(prev_ticker_tokens_idx)
                        prev_tokens = [x for x in prev_tokens if x['token_idx'] > max_id]

                if len(next_tokens) > 0:
                    next_ticker_tokens_idx = [x['token_idx'] for x in next_tokens if x['potential_ticker']]
                    if len(next_ticker_tokens_idx) > 0:
                        min_id = min(next_ticker_tokens_idx)
                        next_tokens = [x for x in next_tokens if x['token_idx'] < min_id]

                current_tokens = [x for x in tokens if (x['sentence_idx'] == sentence_idx)]
                current_tokens_ticker_idx = [x['token_idx'] for x in current_tokens if x['potential_ticker']]
                other_token_ticker_idx = [x for x in current_tokens_ticker_idx if x != token_idx]
                current_tokens = [x for x in current_tokens if x['token_idx'] not in other_token_ticker_idx]

                context_tokens = prev_tokens + current_tokens + next_tokens

                ticker_context = {
                    'raw_text':  token['text'],
                    'ticker': potential_ticker,
                    'token': ticker_token,
                    'context_tokens': context_tokens
                }
                ticker_context_data.append(ticker_context)

        return ticker_context_data

    def simple_method(self, ticker_context):
        """
        Use rule to determine whether the ticker word is a ticker
        :param ticker_context: list of context tokens for ticker decision
        :return:
        """

        ticker_token = ticker_context['token']
        token_idx = ticker_token['token_idx']
        ticker = ticker_context['ticker']
        context_tokens = ticker_context['context_tokens']
        all_text = ' '.join([x['text'] for x in context_tokens])

        # if not all words are capital yet the token is all capital return True
        if ticker_token['isupper'] and not (all_text.isupper()):
            ticker_context['is_ticker'] = True
            return

        # if in near neighbor there is exchange abbrev. word, return True
        no_symbol_tokens = [x for x in context_tokens if not x['is_symbol']]
        prev_tokens = [x for x in no_symbol_tokens if x['token_idx'] < token_idx][-1:]
        after_tokens = [x for x in no_symbol_tokens if x['token_idx'] > token_idx][:1]

        neighbor_tokens_exchange = [x for x in prev_tokens+after_tokens if x['potential_exchange']]

        if neighbor_tokens_exchange:
            ticker_context['is_ticker'] = True
            return

        # if the near before the company name is mentioned, return True
        prev_tokens = [x for x in no_symbol_tokens if x['token_idx'] < token_idx][-3:]
        prev_neighbor_company = [x for x in prev_tokens if x['ticker_company'] == ticker]
        if prev_neighbor_company:
            ticker_context['is_ticker'] = True
            return
