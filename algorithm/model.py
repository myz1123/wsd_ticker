import os
import re
import json
from joblib import dump, load

STOPWORDS_FILE = os.path.join(os.path.dirname(__file__), 'data',
                              'stop_words.json')
STOPWORDS = json.load(open(STOPWORDS_FILE))
VECTORIZER_FILE = os.path.join(os.path.dirname(__file__), 'data',
                               'vectorizer.joblib')
SVM_MODEL_FILE = os.path.join(os.path.dirname(__file__), 'data',
                              'svm_clf.joblib')


def is_useful_token(token):
    """
    Determine whether the token will be used in model
    :param token:  word token
    :return:
    """
    # remove stop words
    if token['lemma'] in STOPWORDS:
        return False

    # remove non alphanumeric symbols
    if token['is_symbol']:
        return False
    if len(token['text']) == 1 and not token['text'].isalnum():
        return False

    # remove time, date identified
    if token['label'] in ('TIME', 'DATE'):
        return False

    # edge case: twitter.com cases removed
    if '.twitter.com' in token['lemma']:
        return False

    return True


def determine_text(x):
    """
    Determine the text word used when building model
    :param x: word token
    :return: word text
    """
    # x% -> percent
    if x['label'] == 'PERCENT':
        return 'percent'

    # money -> money
    if x['label'] == 'MONEY':
        return 'money'

    # num -> number
    if x['label'] in ('QUANTITY', 'CARDINAL'):
        return 'number'

    if x['pos'] == 'NUM':
        return 'number'

    text = re.sub('[^0-9a-zA-Z]+', '', x['lemma'])
    return text


def form_words_set(tokens, ticker):
    """
    form words set passed to the model
    :param tokens: list of tokens dict
    :param ticker: ticker of interest
    :return: list of words, bigram words composed of ticker word and a stop words
    """

    # determine whether to use the token in model
    for t in tokens:
        t['model_text'] = determine_text(t)
        t['useful'] = is_useful_token(t)

    # create bigram word composed of ticker and stop words
    # the reason to do so is that in this task, stop words actually convey a lot of information, if the ticker is
    # indeed a ticker, we generally do not see a stop word around it.
    bigram_added = []
    for idx, t in enumerate(tokens):
        if t['model_text'] == ticker:
            if idx > 0:
                prev_token = tokens[idx - 1]
                # if same sentence
                if (not prev_token['is_symbol']) and (not prev_token['useful']) and \
                        prev_token['sentence_idx'] == t['sentence_idx']:
                    bigram_added.append(prev_token['model_text'] + '_' + ticker)

            if (idx + 1) < len(tokens):
                next_token = tokens[idx + 1]
                if (not next_token['is_symbol']) and (not next_token['useful']) and \
                        next_token['sentence_idx'] == t['sentence_idx']:
                    bigram_added.append(ticker + '_' + next_token['model_text'])

    selected_tokens = [x for x in tokens if x['useful']]
    words = [x['model_text'] for x in selected_tokens]
    return words, bigram_added


class WSDClassifier:

    def __init__(self, vectorizer_file=VECTORIZER_FILE, model_file=SVM_MODEL_FILE):
        """
        create wsd classifier
        :param vectorizer_file:  text to vec file
        :param model_file:  model file
        """
        self.vectorizer = load(vectorizer_file)
        self.model = load(model_file)

    def predict(self, text_data):
        """
        Run model
        :param text_data: list of (list of word tokens)
        :return: result list of is ticker
        """
        X_mtx = self.vectorizer.transform(text_data)
        result = self.model.predict(X_mtx)
        result = [x == 'pos' for x in result]
        return result

    def predict_on_context_tokens(self, ticker_context_tokens_list):
        """
        Run model on text
        :param ticker_context_tokens_list: list of ticker contexts
        :return:
        """
        text_list = []

        for ticker_context in ticker_context_tokens_list:
            # form words set for each ticker context
            context_tokens = ticker_context['context_tokens']
            ticker = ticker_context['ticker']
            words, bigram_added = form_words_set(context_tokens, ticker)
            text = words + bigram_added
            text_list.append(text)

        # run model
        result = self.predict(text_list)

        for idx, ticker_context in enumerate(ticker_context_tokens_list):
            ticker_context['is_ticker'] = result[idx]

        return result