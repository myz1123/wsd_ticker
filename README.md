# WSD TICKER Test #

This is the repo for the WSD Test

### All notebooks are stored in the folder notebook, please configure the python path properly before running them ###

* To donwload model files and training data file: Download relevant data file.ipynb
* Main notebook to run the algo: WST_TICKER.ipynb
* Initial scraping notebooks: Scrape nasdaq.ipnb, Scrape twitter.ipynb
* Build the final model: Model Building Final.ipynb
* Intermediate model building work (Assesment, hypeparameter tunings): Model building - Parameter selection.ipynb
* Feature analysis: Feature Importance

The packages vesions and dependencies are in the file environment.yml

The master branch contains all code handed in for the test on 2018-11-23.

The branch Update_1125 fixed the ugly work around for loading sklearn tfidf vectorizer

In branch Update_1125, there are 2 model building notebooks set:

* Original model building (V2)  
* New model building (V3, cleaned training data): 

	* Model Building V3 Model Building Trace 2018-11-29.ipynb
	* Model Building Final-V3 2018-11-29.ipynb
	* Feature Importance-V3 2018-11-29.ipynb
