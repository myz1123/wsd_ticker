import os
import json
import spacy
import re

CUSTOM_DATA_FILE = os.path.join(os.path.dirname(__file__), 'data',
                                'custom_data.json')

NASDAQ_START = 'Join the Nasdaq Community today and get free, instant access to portfolios, stock ratings, real-time alerts, and more!'
NASDAQ_END = 'The views and opinions expressed herein are the views and opinions of the author and do not necessarily'
NASDAQ_TICKER_RE = re.compile('(\()(Symbol*|[A-Z]*)([ :]*)([A-Z]{3,})(\))')


class TextPreprocessor:

    def __init__(self, nlp=None, custom_data_file=CUSTOM_DATA_FILE):
        """
        create text preprocessor object
        :param nlp: spacy nlp object
        :param custom_data_file:  json file storing some custom data set
        """
        # load custom data
        custom_data = json.load(open(custom_data_file))

        # tickers dict
        self.tickers_dict = custom_data['ticker']

        # exchange dict
        self.exchanges_dict = custom_data['exchanges_dict']

        # regex for finding url
        self.url_re = re.compile(custom_data['url_re'])

        # regex for finding all tickers correspondent company
        self.tickers_company_re = re.compile(custom_data['identified_companies_re'], re.IGNORECASE)

        # tickers companies dict
        self.tickers_company_dict = custom_data['ticker_companies_dict']

        # load nlp object
        if nlp is None:
            self.nlp = spacy.load('en')
        else:
            self.nlp = nlp

    def pre_nlp_process(self, text):
        """
        operation done before going through spacy nlp
        :param text: raw text
        :return: processed text
        """

        # remove all weird characters
        text = re.sub(r'[^\x00-\x7F]', ' ', text)

        # deal with \n
        text = text.replace('\n', ' ')

        # deal with multiple spaces
        text = re.sub('\s+', ' ', text)
        text = re.sub('\( ', '(', text)
        text = re.sub(' \)', ')', text)

        # find all hashtags
        hashtags = re.findall(r'(?<=#)\w+', text)

        # find all at tags
        attags = re.findall(r'(?<=@)\w+', text)

        # find all $ sign tags
        dollartags = re.findall(r'(?<=\$)\w+', text)

        # replace every hashtag from #XXX to XXX_hashtagg
        for tag in hashtags:
            text = text.replace('#' + tag, tag + '_hashtagg', 1)

        # replace every attag from @XXX to XXX_attagg
        for tag in attags:
            text = text.replace('@' + tag, tag + '_attagg', 1)

        # replace every dollartag from $XXX to XXX_dollartagg
        for tag in dollartags:
            if tag.isalpha():
                text = text.replace('$' + tag, tag + '_dollartagg', 1)

        # identify ticker company phrases
        ticker_companies = re.findall(self.tickers_company_re, text)
        # replace every company found to ticker_company
        for company in ticker_companies:
            ticker = self.tickers_company_dict[company.lower()]
            text = text.replace(company, ticker + '_ticker_company', 1)

        # remove url
        text = re.sub(self.url_re, '', text)

        return text

    def post_nlp_process(self, doc):
        """
        operations after spacy nlp process
        :param doc: spacy processed doc
        :return: list of word tokens
        """

        # some initialization
        sentence_idx = 0
        entity_group = []
        tokens_list = []

        # get sentences
        sentences = doc.sents

        # iterate sentences
        for sentence in sentences:

            # ignore all sentences with length 1 and less than 1 character
            ignore_sentence = (len(sentence) == 1 and len(sentence[0]) <= 1)

            if not ignore_sentence:
                # iterate token per sentence
                for token in sentence:
                    # fetch information related to the token
                    token_processed = self.token_processing(token)
                    token_processed['sentence_idx'] = sentence_idx

                    # process entity group based on iob
                    entity_iob = token_processed['entity_iob']

                    # if B or O, the previous accumulate entity list should be processed together
                    if entity_iob in ('B', 'O'):
                        if len(entity_group) > 0:
                            # process entity lit
                            entity_set = self.process_entity_group(entity_group)
                            tokens_list = tokens_list + entity_set
                            # entity group array back to empty
                            entity_group = []

                    # if B or I, token should be inserted to the entity group
                    if entity_iob in ('B', 'I'):
                        entity_group.append(token_processed)
                    else:
                        tokens_list.append(token_processed)

                # process last unprocessed entity group
                if len(entity_group) > 0:
                    # process entity lit
                    entity_set = self.process_entity_group(entity_group)
                    tokens_list = tokens_list + entity_set
                    # entity group array back to empty
                    entity_group = []

                sentence_idx += 1

        tokens_list = [x for x in tokens_list if 'twitter.com' not in x['text']]

        for idx, t in enumerate(tokens_list):
            t['token_idx'] = idx
        return tokens_list

    def token_processing(self, token):
        """
        helper function to extract information per spacy nlp token
        :param token:
        :return: dictionary with relevant info
        """

        # get token info
        token_text = token.text
        lemma = token.lemma_

        # remove _xxxtagg if they were added earlier
        dollartag = '_dollartagg' in token_text
        hashtag = '_hashtagg' in token_text
        attag = '_attagg' in token_text

        if dollartag:
            token_text = token_text.replace('_dollartagg', '')
            lemma = lemma.replace('_dollartagg', '')

        if hashtag:
            token_text = token_text.replace('_hashtagg', '')
            lemma = lemma.replace('_hashtagg', '')

        if attag:
            token_text = token_text.replace('_attagg', '')
            lemma = lemma.replace('_attagg', '')

        lower_text = token_text.lower()
        entity_type = token.ent_type_
        entity_iob = token.ent_iob_
        token_pos = token.pos_
        is_stop_word = token.is_stop

        if lemma in self.tickers_dict:
            lemma = lower_text

        if lower_text in self.tickers_dict:
            potential_ticker = lower_text
        else:
            potential_ticker = ''

        potential_exchange = lower_text in self.exchanges_dict
        if '_ticker_company' in lower_text:
            ticker_company = lower_text.split('_ticker_company')[0]
        else:
            ticker_company = ''

        # is the token upper letter composed
        isupper = token_text.isupper()

        # a token is symbol if it contains no alphanumeric characters
        is_symbol = len(re.findall('\w', token_text)) == 0

        # return
        word_info = {
            'text': token_text,
            'label': entity_type,
            'is_stop': is_stop_word,
            'pos': token_pos,
            'isupper': isupper,
            'dollartag': dollartag,
            'hashtag': hashtag,
            'attag': attag,
            'potential_ticker': potential_ticker,
            'potential_exchange': potential_exchange,
            'ticker_company': ticker_company,
            'lemma': lemma,
            'entity_iob': entity_iob,
            'is_symbol': is_symbol
        }
        return word_info

    def process_entity_group(self, entity_group):
        """
        Given a list of tokens that form a recognized entity,
        determine whether should they be merged together as an entity,
        or they should just be list of individual tokens
        :param entity_group: list of tokens forming an entity
        :return: list of processed tokens dict
        """
        # if the entity tokens contain token that are #@$ tagged or a potential ticker ,
        # return the original list directly
        contain_special_token = [x for x in entity_group if
                                 (x['ticker_company'] != '' or x['potential_exchange'] or
                                  x['potential_ticker'] != '' or x['dollartag'] or x['hashtag'] or x['attag'])]

        if contain_special_token:
            for t in entity_group:
                t['label'] = ''
            return entity_group

        # if the entity is not part of the following labels, return list as no entity identified
        label = entity_group[0]['label']
        if label not in ('TIME', 'DATE', 'PERCENT', 'MONEY', 'QUANTITY', 'CARDINAL'):
            return entity_group

        # otherwise merge all text together to create entity token
        concat_text = '_'.join([x['text'] for x in entity_group])
        concat_token = entity_group[0]
        concat_token['text'] = concat_text

        return [concat_token]

    def nlp_process_batch(self, text_list, batch_size=1000, n_threads=4):
        """
        preprocessing using spacy nlp package
        :param nlp: nlp processor
        :param text_list: list of raw text
        :param batch_size: documents processed batch size
        :param n_threads: number of threads used
        :return: list of raw doc processed
        """
        doc_list = self.nlp.pipe(text_list, batch_size=batch_size, n_threads=n_threads)
        return doc_list

    def text_preprocessing(self, text_list):
        """
        preprocess list of texts
        :param text_list:  list of texts
        :return: per text generate a set of words tokens dict
        """
        texts_list = [self.pre_nlp_process(x) for x in text_list]
        docs_list = self.nlp_process_batch(texts_list)
        tokens_list = [self.post_nlp_process(x) for x in docs_list]
        return tokens_list

    def preprocess_nasdaq_news(self, text_list):
        """
        preprocess nasdaq news
        :param text_list: list of nasdaq news
        :return:
        """
        texts_data = [{'text': self.pre_nlp_process(x)} for x in text_list]
        for data in texts_data:
            tickers = re.findall(NASDAQ_TICKER_RE, data['text'])
            tickers = [''.join(x) for x in tickers]
            tickers_dict = {}
            for t in tickers:
                ticker = t.split(':')
                if len(ticker) > 1:
                    ticker = ticker[1]
                else:
                    ticker = ticker[0]
                ticker = re.sub('[^\w]', '', ticker)
                tickers_dict[t] = ticker.lower()
                tickers_dict[ticker] = ticker.lower()

            data['tickers_dict'] = tickers_dict
            reg_str = '|'.join(tickers_dict.keys())
            reg_str = reg_str.replace('(', '\(')
            reg_str = reg_str.replace(')', '\)')
            data['tickers_re'] = re.compile(reg_str)

        docs_list = self.nlp_process_batch([x['text'] for x in texts_data])
        for idx, doc in enumerate(docs_list):
            doc_sentences = []
            data = texts_data[idx]
            sentences = list(doc.sents)
            for s in sentences:
                sentence_text = s.text
                ents = list(s.ents)
                orgs = []
                for e in ents:
                    if e.label_ == 'ORG':
                        org_text = e.text
                        if org_text not in data['tickers']:
                            orgs.append(org_text)
                tickers = []
                if len(data['tickers_dict']) > 0:
                    tickers = re.findall(data['tickers_re'], sentence_text)

                sentence_data = {
                    'text': sentence_text,
                    'tickers': tickers,
                    'orgs': orgs
                }
                doc_sentences.append(sentence_data)
            data['sentences'] = doc_sentences

        return texts_data

    def nasdaq_text_striping(self, text):
        """
        strip first and last sentence of nasdaq news
        :param text:
        :return:
        """
        text = text.split(NASDAQ_START)
        if len(text) > 1:
            text = text[1]
        else:
            text = text[0]

        text = text.split(NASDAQ_END)[0]

        return text
